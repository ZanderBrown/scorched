/* window.vala
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Scorched {
	[GtkTemplate (ui = "/org/gnome/zbrown/Scorched/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		[GtkChild]
		unowned Gtk.Label temp;

		[GtkChild]
		unowned Gtk.Stack stack;
		[GtkChild]
		unowned Gtk.ListBox zones;

		HashTable<string, Gtk.Label> rows = new HashTable<string, Gtk.Label>(str_hash, str_equal);

		public Window (Application app) {
			Object (application: app);

			app.sensor_data.connect ((type_line, c) => {
				if (rows[type_line] == null) {
					var row = new Adw.ActionRow () {
						title = type_line
					};

					var val = new Gtk.Label (null) {
						xalign = 1,
					};
					val.add_css_class ("numeric");
					row.add_suffix (val);

					rows[type_line] = val;
					zones.append (row);
				}
				rows[type_line].label = "%i ℃".printf(c);
				stack.visible_child = zones;
			});

			app.temp.connect (degrees => {
				temp.label = "%i".printf(degrees);
			});
		}

		[GtkCallback]
		private void about () {
			var developers = new string[] { "Zander Brown" };
			var dlg = new Adw.AboutWindow () {
				transient_for = this,
				application_name = Environment.get_application_name (),
				application_icon = "org.gnome.zbrown.Scorched",
				version = PACKAGE_VERSION,
				copyright = _("© 2019 Zander Brown"),
				license_type = GPL_3_0,
				developer_name = "Zander Brown",
				developers = developers,
				website = "https://gitlab.gnome.org/ZanderBrown/scorched",
				issue_url = "https://gitlab.gnome.org/ZanderBrown/scorched/-/issues/new"
			};
			dlg.present ();
		}
	}
}
