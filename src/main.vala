/* main.vala
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Scorched {
	const string THERMAL =  "thermal_zone";

	public class Application : Adw.Application {
		public string[] ignored { get; set; }

		private Settings settings;
		uint timeout = 0;
		File thermal_dir = null;

		public signal void sensor_data (string sensor, int degrees);
		public signal void temp (int degrees);

		construct {
			application_id = "org.gnome.zbrown.Scorched";
			flags = ApplicationFlags.FLAGS_NONE;

			settings = new Settings ("org.gnome.zbrown.Scorched");
			settings.bind ("ignored", this, "ignored", DEFAULT);

			notify["ignored"].connect (() => {
				// TODO: Implement this
			});

			timeout = Timeout.add (500, () => {
				update.begin ((obj, res) => {
					try {
						update.end (res);
					} catch (Error e) {
						critical ("Problem fetching data: %s", e.message);
					}
				});
				return Source.CONTINUE;
			});

			thermal_dir = File.new_for_path ("/sys/class/thermal/");

			style_manager.color_scheme = FORCE_DARK;
		}

		~Application () {
			Source.remove (timeout);
		}

		public override void activate () {
			var win = this.active_window ?? new Scorched.Window (this);

			win.present ();
		}

		private async void update () throws GLib.Error {
			var values = new GenericArray<int> ();
			var children = yield thermal_dir.enumerate_children_async ("standard::name", NOFOLLOW_SYMLINKS);
			FileInfo info;
			while ((info = children.next_file (null)) != null) {
				var name = info.get_name ();

				if (name.has_prefix (THERMAL)) {
					var sensor_dir = thermal_dir.get_child (name);
					var temp_file = sensor_dir.get_child ("temp");
					var temp_stream = yield temp_file.read_async ();
					var temp_data = new DataInputStream (temp_stream);
					var temp_line = yield temp_data.read_line_async ();
					var milic = int.parse (temp_line);
					var c = (int) Math.round (milic / 1000);

					var type_file = sensor_dir.get_child ("type");
					var type_stream = yield type_file.read_async ();
					var type_data = new DataInputStream (type_stream);
					var type_line = yield type_data.read_line_async( );

					if (type_line in ignored) {
						continue;
					}

					values.add(c);

					sensor_data (type_line, c);
				}
			}
			var sum = 0;
			values.foreach (t => {
				sum += t;
			});
			temp (sum / values.length);
		}
	}
}

int main (string[] args) {
	Intl.setlocale (LocaleCategory.ALL, "");
	Intl.bindtextdomain (GETTEXT_PACKAGE, LOCALE_DIR);
	Intl.bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	Intl.textdomain (GETTEXT_PACKAGE);

	Environment.set_application_name (_("Scorched"));
	Environment.set_prgname ("org.gnome.zbrown.Scorched");

	Gtk.Window.set_default_icon_name ("org.gnome.zbrown.Scorched");

	var app = new Scorched.Application ();

	return app.run (args);
}
